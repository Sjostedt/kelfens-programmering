
## Kod fÃ¶r lab3

```Python
from tkinter import *

# https://docs.python.org/3/library/tkinter.html

class Kryssruta(Button):
    """ Knapp som kryssas i/ur nÃ¤r man trycker pÃ¥ den """
    
    def __init__(self, master = None, nr = 0):
        Button.__init__(self, master)
        self.master = master
        self.nr = nr
        self.antaltryck = 0
        self["command"] = self.nedtryckt
        self.bind("<Enter>", self.fokus)
    
    def nedtryckt(self):
        self.antaltryck += 1
        self["text"] = self.antaltryck
        rest = self.antaltryck % 3
        if rest == 0:
            self["bg"] = "lightgreen"
        elif rest == 1:
            self["bg"] = "pink"
        else:
            self["bg"] = "lightblue"
        self.master.uppdateraInfo(self.nr);

    def fokus(self, event):
        self.master.inforad["text"] = "Knapp " + str(self.nr) + " Ã¶vervÃ¤gs   "
        
class KnappMatris(Frame):
    """ En lista med kryssrutor som ritas som en matris """

    def __init__(self, master = None, rader = 1, kolumner = 5):
        Frame.__init__(self, master)
        self.grid()
        self.rader = rader
        self.kolumner = kolumner
        self.antal = rader*kolumner
        self.skapaKryssrutor()
        self.inforad = Label(master, text = "Info kan skrivas hÃ¤r")
        self.pynta(self.inforad, bredd = (self.kolumner+2)*3)
        #self.inforad.grid(row = self.rader, column = 0)
        self.inforad.grid()
        
    def pynta(self, komponent, bakgrundsfarg = "white", bredd = 3, hojd = 1, textfarg = "black", font = ("Times", 20, "normal")):
        komponent["width"] = bredd
        komponent["height"] = hojd
        komponent["bg"] = bakgrundsfarg
        komponent["fg"] = textfarg
        komponent["font"] = font
        komponent["highlightbackground"] = "purple"
        komponent["highlightcolor"] = "green"
        komponent["highlightthickness"] = 2

        
    def skapaKryssrutor(self):
        self.knapplista = []
        for nr in range(self.antal):
            rad = nr//self.kolumner
            kolumn = nr%self.kolumner
            ny = Kryssruta(self, nr)  
            self.pynta(ny, bakgrundsfarg="grey")
            ny.grid(row = rad, column = kolumn)
            self.knapplista.append(ny)

    def uppdateraInfo(self, id):
        self.inforad["text"] = "Ruta " + str(id) + " trycktes "
            

def main():
    rot = Tk()
    matris = KnappMatris(rot, 2)
    matris.mainloop()

if __name__ == "__main__":
    main()
```

