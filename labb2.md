### Labb 2 Filhantering och matriser (Python)

I den hÃ¤r labben ska du skriva flera versioner av samma program. Ha med ett lÃ¶pnummer i filnamnet som du Ã¶kar allt eftersom sÃ¥ att man kan prata om hur utvecklingen har gÃ¥tt framÃ¥t vid redovisning.
```
labb2_1.py
labb2_2.py
...
```
LÃ¤s om *filer (files)*, *listor (lists)* och *funktioner (functions)* i valfri python bok. 

[En text of funktioner i python](https://www.python-course.eu/python3_functions.php)

[och om filer](https://www.python-course.eu/python3_file_management.php)

[HÃ¤r finns en mycket kort sammanfattning of listor](http://www.pythonforbeginners.com/basics/python-list-manipulation)

[HÃ¤r finns en mycket kort sammanfattning of filer](http://www.pythonforbeginners.com/cheatsheet/python-file-handling)


_________________________________________________________________________
### Uppgifter

#### Uppgift 1
- Skriv ett litet program som lÃ¤ser in valfritt data (som du hittar pÃ¥) frÃ¥n fil till en vektor/lista och skriver ut pÃ¥ skÃ¤rmen. SÃ¥ hÃ¤r skulle en fÃ¶rsta version kunna se ut:

Fil	Utskrift
```
# Exempeldata
Alexander MDI 95 1
Linda TCS 96 2
```

```
>>['Alexander', 'MDI', '95', '2']
>>['Linda', 'TCS', 9276, '2']
```

Snygga till ditt program med begripliga utskrifter (OBS det Ã¤r du sjÃ¤lv som hittat pÃ¥ data och vet vad de stÃ¥r fÃ¶r).

- LÃ¤gg till en if-sats sÃ¥ att filrader som bÃ¶rjar med ett speciellt (vÃ¤lj sjÃ¤lv) tecken inte skrivs ut.

#### Uppgift 2

- Nu ska du vidareutveckla programmet du skrev i labb 1. Skriv en ny fil dÃ¤r du [*importerar*](https://docs.python.org/3/reference/simple_stmts.html#import) funktionerna frÃ¥n fÃ¶rra labben.

- Testfallen i labb1 1 var hÃ¥rdkodade. Spara det hÃ¥rdkodade testdatat i labb 1 i en textfil.

- Skriv en ny funktion som lÃ¤ser in testdatat fÃ¶r labb 1 frÃ¥n fil. 


- Skriv en utskriftsfunktion som skÃ¶ter utskrifterna. Ditt huvudprogram (fÃ¶r ett testfall) skulle nu kunna se ut ungefÃ¤r sÃ¥ hÃ¤r:
```Python
     tecken, vektor, forvantat_utfall = parsefile()
     result = checkvector(tecken, vektor, forvantat_utfall)
     presentera_resultat_snyggt(result, tecken, vektor,forvantat_utfall)
```

- I pythonprogram finns en if-sats som ser ut som nedan, vad har man den till?

```Python
      if __name__ == "__main__":
```
  

#### Uppgift 3
Nedan Ã¤r lite kod du kan prova att kÃ¶ra sjÃ¤lv fÃ¶r att bekanta dig med nÃ¤stlade listor.
```
>>> en_matris = []
>>> en_rad = "a b d 3 2 2"
>>> en_matris.append(en_rad.split())
>>> en_matris.append(en_rad.split())
>>> en_matris
[['a', 'b', 'd', '3', '2', '2'], ['a', 'b', 'd', '3', '2', '2']]
```

LÃ¤s om listmetoden [append()](https://docs.python.org/3.6/tutorial/datastructures.html) och strÃ¤ngmetoden [split()](https://docs.python.org/3/library/string.html#string.split) i Pythons dokumentation. 
Spara lÃ¤nken till Pythons dokumentation - i fortsÃ¤ttningen kan du sjÃ¤lv slÃ¥ upp metoder som du vill veta mer om!


- Skriv en ny version av ditt program som kan hantera matriser. Programmet ska kontrollera fem i rad lodrÃ¤tt och vÃ¥grÃ¤tt. Skriv hÃ¥rdkodade testfall.
Exempel pÃ¥ hÃ¥rdkodad talmatris:
```
 m = [[1, 3, 5] , [1, 24, 8], [1, 6, 17]]
```

#### Uppgift 4
- UtÃ¶ka med diagonal koll av fem 'X' i rad. Skriv hÃ¥rdkodade testfall med, precis som i fÃ¶rra labben, utskrifter av vad som testas (den hÃ¥rdkodade matrisen), fÃ¶rvÃ¤ntat resultat och utfall.

#### Uppgift 5
- Skriv en ny version av ditt program som lÃ¤ser testdata frÃ¥n fil. Du fÃ¥r sjÃ¤lv bestÃ¤mma filformatet.

- Hur mÃ¥nga tester behÃ¶ver kÃ¶ras fÃ¶r att man ska lita pÃ¥ att funktionaliteten Ã¤r som den ska? 

- Har du nÃ¥gra kÃ¤nda begrÃ¤nsningar i ditt program? Skriv ner dem i sÃ¥ fall och redovisa vid redovisningen!

#### Uppgift 6
- Skriv en slutversion av programmet. Det Ã¤r denna version du ska redovisa, men spara de Ã¤ldre versionerna sÃ¥ att det gÃ¥r att diskutera hur programmet vÃ¤xte fram. Denna version ska vara uppdelad i funktioner, bland annat: 
 - En funktion som lÃ¤ser in testdata frÃ¥n fil och returnerar data i valfritt format. 
 - En annan funktion som tar dessa data som inparametrar och i sin tur anropar olika testfunktioner.

- Titta efter om nÃ¥got Ã¤r hÃ¥rdkodat i ditt program. Anteckna vad i sÃ¥ fall och redovisa vid redovisningen!

_________________________________________________________________________
### Redovisning

Inskickade labbar redovisas muntligt, bokningsinstruktioner kommer pÃ¥ kurswebbsidan. Ta underskrift av handledaren efter godkÃ¤nd redovisning. 

