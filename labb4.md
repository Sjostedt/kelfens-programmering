## Labb 4  Fem-i-rad i repris (C)


### LÃ¤s om pekare

LÃ¤s igenom [Ted Jensens tutorial](http://pweb.netcom.com/~tjensen/ptr/pointers.htm) om pekare i C, framfÃ¶r allt kap 1, 2 och 7.
Svara pÃ¥ fÃ¶ljande frÃ¥gor och redovisa svaren vid redovisningen.
- 1. Vad menas i kapitel tvÃ¥ med den hÃ¤r raden? ptr = &my_array[0];
- 2. Vad Ã¤r skillnaden mellan de tvÃ¥ variablerna?
- 3. Vad betyder 3[a]?
- 4. Vad Ã¤r skillnaden mellan *(*(multi + row) + col) och multi[row][col]?
 
### LÃ¤s om printf, format och heltal i C-hÃ¤ftet (eller pÃ¥ nÃ¤tet) 

Kompilera och kÃ¶r nedanstÃ¥ende program. 
Skriv ner fÃ¶rklaringar till varfÃ¶r utskrifterna blir som de blir. 
 ```c
#include <limits.h>
#include <stdio.h>
int main(char ** argv, int argc) 
	{
		short s = 32760;
  		for (int i = 0; i < 12; ++i)
    	{
    		printf("s = %d\n", s++);
  		}

  		int x = INT_MAX - 6;
  		for (int i = 0; i < 12; ++i)
  		{
    		printf("x = %d\n", x++);
  		}
  
  		unsigned int y = 5;
 		for (int i = 0; i < 12; ++i)
 		{
    		printf("y = %u\n", y--); 
    	}
	}//main
```
Skulle det bli nÃ¥gon skillnad om man byter ut %u mot %d i sista printf-satsen?
### Uppgift

Skriv ett C-program som lÃ¤ser in ett testfall frÃ¥n fil och testar om det finns 5 i rad. Det rÃ¤cker med ett testfall men det ska vara ett dÃ¤r fem i rad inte hittas. Logiken har du redan skrivit i python. Utskriften ska vara ungefÃ¤r densamma som i pytonlabben. 

SÃ¥ hÃ¤r skulle en utskrift av programmet kunna se ut.

```
>>LÃ¤ser in matris frÃ¥n filen "matristest.txt":
>>_ X _ _ _ _ _ X
>>_ _ _ _ _ X _ _ 
>>_ _ X X X _ _ X
>>_ X X _ _ _ _ _ 
>>_ _ X _ X X X _ 
>>
>>Testar om det finns fem i rad.
>>FÃ¶rvÃ¤ntat resultat: Negativt
>>
>>BÃ¶rjar testen
>>   Inga fem i rad lodrÃ¤tt
>>   Inga fem i rad vÃ¥grÃ¤tt
>>   Inga fem i rad diagonalt
>>Resultat: Negativt
```

Till din hjÃ¤lp finns det C-program (fÃ¶relÃ¤sning 7 [FillÃ¤sning i C](http://www.csc.kth.se/utbildning/kth/kurser/DD1321/tilpro12/anteckningar/fil.c). av data.txt)) som lÃ¤ser in en matris frÃ¥n fil och skriver ut matrisen samt ett C-program (Ã¶vning [odd_matrix.c](http://www.csc.kth.se/utbildning/kth/kurser/DD1321/tilpro12/anteckningar/odd_matrix.c) ) som skriver ut jÃ¤mna tal i en matris.

TÃ¤nk pÃ¥ att nÃ¤r du kompilerat ditt program mÃ¥ste du kÃ¶ra det ocksÃ¥. Om du inte namnger vad programmet ska heta med flaggan -o sÃ¥ dÃ¶ps programmet till a.out pÃ¥ unix och a.exe pÃ¥ windows.
```
 >> gcc mittprogram.c
 >> ./a.out
```
### Redovisning

Inskickade labbar redovisas muntligt, bokningsinstruktioner kommer pÃ¥ kurswebbsidan. Ta underskrift av handledaren efter godkÃ¤nd redovisning. 

