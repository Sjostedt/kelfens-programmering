## Labb 5  GUI & nÃ¤tverk (Python)

### GUI i Python med tkinter

GÃ¶r ett program med grafiskt grÃ¤nssnitt dÃ¤r man kan spela fem-i-rad med sin labbkamrat.

### FÃ¶rberedelse

UtgÃ¥ frÃ¥n labb3. LÃ¤s pÃ¥ om grafik, arv, hÃ¤ndelseorienterad programmering efter behov. 

### Uppgifter
#### Uppgift 1: Kryssprogram med GUI


Skriv ett program som ritar upp en matris med knappar (Button) och markerar med 'X' dÃ¤r man klickar med musen.

ProvkÃ¶r programmet.

Titta pÃ¥ koden och var noga med att fÃ¶rstÃ¥ hur det fungerar.

Experimentera lite genom att gÃ¶ra smÃ¥ Ã¤ndringar i programmet. Prova nÃ¥gon annan hÃ¤ndelse, t.ex. om man pekar pÃ¥ en knapp utan att trycka.

#### Uppgift 2: TvÃ¥ spelare

Ãndra i programmet sÃ¥ att det blir 'X' varannan gÃ¥ng man trycker och 'O' varannan gÃ¥ng.

#### Uppgift 3: Vem vann?

Importera dina funktioner frÃ¥n labb 2 ocn anvÃ¤nd dem sÃ¥ att det efter varje tryckning kollas om nÃ¥gon av spelarna har fÃ¥tt fem i rad. 
AnvÃ¤nd info-raden fÃ¶r att tala om nÃ¤r nÃ¥gon har vunnit.
[SÃ¥ hÃ¤r skulle det kunna se ut:](http://www.csc.kth.se/utbildning/kth/kurser/DD1321/tilpro12/labbar/femOirad.jpg) 

#### Uppgift 4: Spela Ã¶ver nÃ¤tverk 

Skriv en tidsuppskattning fÃ¶r att implementera en spelprototyp som gÃ¥r att spela Ã¶ver nÃ¤tverk. LÃ¤s pÃ¥ om [socketprogrammering](https://docs.python.org/3/howto/sockets.html). Det finns lite olika sÃ¤tt att lÃ¶sa nÃ¤tverksprogrammeringen. Endera kan ena grafikprogrammet agera server och det andra programmet klient. Skicka data mellan programmen med module pickle som paketerar datat i teckenformat. HÃ¤r Ã¤r ett exempel pÃ¥ en enkelt [klient](http://www.csc.kth.se/utbildning/kth/kurser/DD1321/tilpro13/labbar/client2.py) och [server](http://www.csc.kth.se/utbildning/kth/kurser/DD1321/tilpro13/labbar/server2.py) frÃ¥n nÃ¤tet som modifierats en aning. Notera att du kan paketera allt du behÃ¶ver skicka med pickle.dumps.

Ett annat sÃ¤tt att lÃ¶sa uppgiften Ã¤r att implementera en separat server och lÃ¥ta bÃ¥da grafikprogrammen ansluta sig som klienter. NÃ¤r den fÃ¶rsta klienten ansluter sÃ¥ kan ett meddelande typ waiting for other player skrivas ut. Servern behÃ¶ver inte gÃ¶ra mycket mer Ã¤n att skicka inkommande data till den andra klienten. HÃ¤r Ã¤r en minimal [chatserver](http://www.csc.kth.se/utbildning/kth/kurser/DD1321/tilpro13/labbar/serverAlexander.py) som man kan ansluta sig till med telnet (telnet localhost 51234). Notera att det enda servern gÃ¶r Ã¤r att skyffla data frÃ¥n den ena klienten till den andra. 

Du ska inte implementera koden utan gÃ¶ra en *tidsuppskattning* pÃ¥ hur lÃ¥ng tid det skulle ta fÃ¶r dig att lÃ¤ra dig det du behÃ¶ver fÃ¶r att lÃ¶sa uppgiften, implementera koden samt testa koden.

### Krav fÃ¶r godkÃ¤nt

- Ett grafiskt fem-i-rad spel som Ã¤r intuitivt att spela
- Spelet fÃ¥r inte vara hÃ¥rdkodat, det ska lÃ¤tt kunna Ã¤ndras till annan storlek, rektangulÃ¤r eller fyra i rad.
- En tidsuppskattning fÃ¶r nÃ¤tverksspel

### Redovisning

Inskickade labbar redovisas muntligt, bokningsinstruktioner kommer pÃ¥ kurswebbsidan. Ta underskrift av handledaren efter godkÃ¤nd redovisning. 

