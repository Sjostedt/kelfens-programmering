# DD1321-HT-labbar

Labbar DD1321 fÃ¶r hÃ¶stterminen moment LAB1

## Labb 1 Fem-i-rad (Python)

### FÃ¶rberedelse

LÃ¤s pÃ¥ valfri bok om python. 

[LÃ¤nk till kort python introduktion](https://en.wikibooks.org/wiki/A_Beginner%27s_Python_Tutorial)

### Uppgifter


#### Uppgift 1

- Skriv en funktion som kontrollerar om en vektor har fem X (tecknet 'X') i rad. 

- Skriv ett huvudprogram som testar din funktion och redovisar resultaten i terminalfÃ¶nstret. Testutskrifterna ska innehÃ¥lla vad som testas, fÃ¶rvÃ¤ntat utfall samt resultatet. HÃ¥rdkoda vektorerna och skriv ett testfall dÃ¤r utfallet Ã¤r sant och ett testfall dÃ¤r utfallet Ã¤r falskt.

SÃ¥ hÃ¤r skulle utskriften kunna se ut:
```Python
Testar 5 'X' i rad i vektorn ['1', '3', 'X', 'X', 'X', 'X', 'X', 'W']
FÃ¶rvÃ¤ntat utfall: True
Utfall: True
```

- Vilka andra testfall kan man tÃ¤nka sig?

#### Uppgift 2

- Skriv en funktion som kontrollerar om en vektor har fem O (tecknet 'O') i rad. 

- Skriv ett huvudprogram som testar din funktion och redovisar resultaten i terminalfÃ¶nstret. Testutskrifterna ska innehÃ¥lla vad som testas, fÃ¶rvÃ¤ntat resultat samt resultatet. HÃ¥rdkoda vektorerna och skriv ett testfall dÃ¤r utfallet Ã¤r sant och ett testfall dÃ¤r utfallet Ã¤r falskt.

- Hur enkelt Ã¤r det att modifiera ditt program fÃ¶r godtycklig fem-i-rad-teckensÃ¶kning?

_________________________________________________________________________

### Redovisning

Labb 1 behÃ¶ver inte skickas in men ska redovisas muntligt, bokningsinstruktioner kommer pÃ¥ kurswebbsidan. Ta underskrift av handledaren efter godkÃ¤nd redovisning. 

