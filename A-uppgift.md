
###A-uppgift
FÃ¶r det slutgiltiga betyget pÃ¥ DD1321 behÃ¶ver man gÃ¶ra en individuell A-uppgift. Observera att man Ã¤ven mÃ¥ste gÃ¶ra och klara en C-uppgift fÃ¶r betyg A.
Uppgiften bestÃ¥r i att programmera labb 5 med nÃ¤tverk.

####Krav
- Spelet ska vara intuitivt att spela och fÃ¶rstÃ¥ (se muntan om anvÃ¤ndarvÃ¤nlighet)
- Det ska vara mÃ¶jligt att gÃ¶ra instÃ¤llningar i en konfigurationsfil t.ex. hur stor spelplanen ska vara (7x9, 5x6), tecken att spela med och eventuellt (frivillig) antal i rad fÃ¶r att vinna
- Man ska kunna ha rektangulÃ¤ra spelplaner
- Spellogiken ska vara separerad frÃ¥n anvÃ¤ndargrÃ¤nssnittet. AnvÃ¤nd metoder/funktioner.
- Det ska finnas en timer som visar tiden man vÃ¤ntar pÃ¥ motspelaren/gÃ¶r sitt drag. Skriv separat kod (klass) fÃ¶r detta.
- Uppgiften ska lÃ¤mnas in senast februari och redovisas dÃ¤refter.
- LÃ¤s pÃ¥ om trÃ¥dar och fÃ¶rklara hur ditt program fungerar. 
- Du ska ha nÃ¥gon slags extra funktionalitet, t.ex. sammanlagd tid, mouse-over, menyer eller tipsfunktion, som skiljer din implementation frÃ¥n Ã¶vriga inlÃ¤mningar. AnvÃ¤nd din fantasi.