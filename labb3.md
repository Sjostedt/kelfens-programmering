## Labb 3  GUI & nÃ¤tverk (Python)

### GUI i Python med tkinter

GÃ¶r ett program med grafiskt grÃ¤nssnitt dÃ¤r man kan

### FÃ¶rberedelse

LÃ¤s fÃ¶rst om arv och om tkinter i valfri pythonbok:

[En lÃ¤gre text om klasser och arv](https://www.python-course.eu/python3_inheritance.php)

[tkinter dokumentation](https://docs.python.org/3/library/tkinter.html)


### Uppgifter

#### Uppgift 1

LÃ¤s

[effbots introduktion till tkinter](http://effbot.org/tkinterbook/tkinter-hello-tkinter.htm). OBS python 2 byt Tkinter till tkinter med litet 't' fÃ¶r python 3.

Skriv in och provkÃ¶r exemplet "Hello world".
Svara sedan pÃ¥ fÃ¶ljande frÃ¥gor.

1. FÃ¶rklara vad mainloop() Ã¤r.
2. Hur mÃ¥nga attribut har en [Button](http://effbot.org/tkinterbook/button.htm)?
3. Skriv ner tre exempel pÃ¥ attribut i Button!
4. Vilka metoder har en Button?
5. Titta pÃ¥ klassen Frame. Finns det nÃ¥got attribut i [Frame](http://effbot.org/tkinterbook/frame.htm) som har samma namn som nÃ¥got av attributen i Button?


#### Uppgift 2

Studera och provkÃ¶r 

[grafik fÃ¶r labb3.py](grafiklabb3.md)

GÃ¶r Ã¤ndringar i programmet. LÃ¤gg till fÃ¤rger. Ãndra beteenden. 


#### Uppgift 3

UtgÃ¥ frÃ¥n programmet ovan och gÃ¶r ett nytt program. NÃ¤r man trycker pÃ¥
en knapp ska en eller flera *andra* knappar Ã¤ndras enligt ett beteende
som du sjÃ¤lv definierar. Exempel, varannan knapp Ã¤ndras, knappen
nedanfÃ¶r och till hÃ¶ger Ã¤ndras. Knappar som har samma fÃ¤rg som knappen till hÃ¶ger Ã¤ndras. 

TIPS, fÃ¶r att komma Ã¥t andra knappar frÃ¥n en knapp. AnvÃ¤nder self.master fÃ¶r att nÃ¥ Knappmatris. 
FrÃ¥n Knappmatris kan du pÃ¥verka andra knappar. 

#### Uppgift 4

Dokumentera vilka medlemsvariabler som mÃ¥ste kollas av fÃ¶r att dina beteenden ska fungera. 


#### Uppgift 5

Skriv minst tre testdokument dÃ¤r du beskriver nÃ¥gra knapptryckningsfÃ¶ljder och vad det fÃ¶rvÃ¤ndate resultatet ska vara. AnvÃ¤nd skÃ¤rmdumpar fÃ¶r att Ã¥skÃ¥dliggÃ¶ra det fÃ¶rvÃ¤ntade resultatet.


### Redovisning

Inskickade labbar redovisas muntligt, bokningsinstruktioner kommer pÃ¥ kurswebbsidan. Ta unde
